export const environment = {
  production: true,
  postsUrl: 'https://jsonplaceholder.typicode.com/posts',
  imagePath: 'https://assetsdelivery.eldorado.gg/v7/_assets_/miscellaneous/v1/article-no-image.jpg'
};
