import { createReducer, on } from "@ngrx/store";
import { Article } from "src/app/models/articles.model";
import * as ArticlesActions from "./articles.actions";

export interface ArticlesState {
  articles: Article[];
}

export const initialState: ArticlesState = {
  articles: []
};
// needs reset and success to change state
export const articlesReducer = createReducer(
  initialState,
  on(ArticlesActions.getArticlesAction, state => ({ ...state })),
  on(ArticlesActions.getArticlesSuccessAction, state => ({ ...state })),
  on(ArticlesActions.getArticlesFailedAction, state => ({ ...state })),
  on(ArticlesActions.setArticlesAction, (state, { payload }) => ({
    articles: [...payload.articles]
  }))
);


