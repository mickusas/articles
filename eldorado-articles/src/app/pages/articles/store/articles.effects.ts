import * as ArticlesActions from './articles.actions';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { catchError, map, switchMap } from 'rxjs/operators';
import { ArticlesService } from 'src/app/services/articles.service';
import { of } from 'rxjs';
import { Article } from 'src/app/models/articles.model';

@Injectable()
export class ArticlesEffects {

  constructor(
    private actions$: Actions<Action>,
    private articlesService: ArticlesService) { }

  public getArticles$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ArticlesActions.getArticlesAction),
      switchMap(() => {
        return this.articlesService.getArticles().pipe(
          map((response: Article[]) => {
            ArticlesActions.getArticlesSuccessAction;
            return ArticlesActions.setArticlesAction({ payload: { articles: response } });
          }),
          catchError(() => of(ArticlesActions.getArticlesFailedAction()))
        );
      })
    );
  });
}
