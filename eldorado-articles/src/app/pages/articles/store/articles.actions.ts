import { props, createAction } from "@ngrx/store";
import { Article } from "src/app/models/articles.model";

export const GET_ARTICLES = "[Articles] Get Articles";
export const GET_ARTICLES_SUCCESS = "[Articles] Get Articles Success";
export const GET_ARTICLES_FAIL = "[Articles] Get Articles Fail";
export const SET_ARTICLES = "[Articles] Set Articles";

export const getArticlesAction = createAction(GET_ARTICLES);
export const getArticlesSuccessAction = createAction(GET_ARTICLES_SUCCESS); // setArticles has to be success
export const getArticlesFailedAction = createAction(GET_ARTICLES_FAIL);
export const setArticlesAction = createAction(
  SET_ARTICLES,
  props<{
    payload: {
      articles: Article[];
    };
  }>()
);
