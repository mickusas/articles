import { Component, Input, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Store } from '@ngrx/store';
import * as fromApp from '../../store/app.reducer';
import * as ArticlesActions from './store/articles.actions';
import { Observable } from 'rxjs';
import { ArticlesState } from './store/articles.reducer';
import { Article } from 'src/app/models/articles.model';

@Component({
  selector: 'app-article',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit {
  imagePath = environment.imagePath;
  articles$: Observable<{ articles: Article[] }>;

  constructor(
    private store: Store<fromApp.AppState>
  ) {
    this.articles$ = this.store.select('articles');
  }

  ngOnInit(): void {
    this.store.dispatch(ArticlesActions.getArticlesAction());
  }
}
