import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArticlesComponent } from './articles.component';



const routes: Routes = [
  {
    path: 'article/:id',
    loadChildren: () => import('../article/article.module').then(m => m.ArticleModule)
  },
  {
    path: 'submit',
    loadChildren: () => import('../main-article-edit/main-article-edit.module').then(m => m.MainArticleEditModule)
  },
  {
    path: 'main-article',
    loadChildren: () => import('../main-article/main-article.module').then(m => m.MainArticleModule)
  },
  {
    path: '',
    component: ArticlesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticlesRoutingModule { }
