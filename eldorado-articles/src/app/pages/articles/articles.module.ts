import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticlesComponent } from './articles.component';
import { ArticlesRoutingModule } from './articles-routing.module';
import { TruncatePipe } from 'src/app/pipes/truncate.pipe';
import { HighlightDirective } from './highlight.directive';


@NgModule({
  declarations: [ArticlesComponent, TruncatePipe, HighlightDirective],
  imports: [
    CommonModule,
    ArticlesRoutingModule,
  ]
})
export class ArticlesModule { }
