import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Article, ArticleView } from 'src/app/models/articles.model';
import { select, Store } from '@ngrx/store';
import * as ArticleActions from './store/article.actions';
import { Observable, Subject } from 'rxjs';
import * as fromApp from '../../store/app.reducer';
import { getArticle } from './store/article.selectors';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit, OnDestroy {
  article$: Observable<Article>;
  articleView: ArticleView;
  destroy = new Subject();

  constructor(
    private store: Store<fromApp.AppState>,
    private route: ActivatedRoute
  ) {
    this.store.dispatch(ArticleActions.getArticleAction({ id: this.route.snapshot.params.id })); // better subscribe
    this.article$ = this.store.pipe(select(getArticle));
  }

  ngOnInit() {
    this.subscribeArticle();
  }

  subscribeArticle(): void {
    this.store.pipe(
      select(getArticle),
      takeUntil(this.destroy)
    ).subscribe((article: Article) => {
      if (article) {
        this.articleView = {
          title: article.title,
          imagePath: article.imagePath,
          body: article.body
        };
      }
    });
  }

  ngOnDestroy() {
    this.destroy.next()
    this.destroy.complete()
    this.store.dispatch(ArticleActions.resetArticleAction());
  }
}

