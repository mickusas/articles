import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleComponent } from './article.component';
import { ArticleRoutingModule } from './article-routing.module';
import { ArticleViewModule } from 'src/app/shared/article-view/article-view/article-view.module';

@NgModule({
  declarations: [ArticleComponent],
  imports: [
    CommonModule,
    ArticleViewModule,
    ArticleRoutingModule,
  ]
})
export class ArticleModule { }
