import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Article } from "src/app/models/articles.model";

export const getArticleFeatureState = createFeatureSelector<{ article: Article }>("article");

export const getArticle = createSelector(
  getArticleFeatureState,
  (state: { article: Article }) => state?.article
)
