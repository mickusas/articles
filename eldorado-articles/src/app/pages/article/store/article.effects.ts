import { Injectable } from '@angular/core';
import * as ArticleActions from './article.actions';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap, map, catchError } from 'rxjs/operators';
import { ArticlesService } from '../../../services/articles.service';
import { of } from 'rxjs';
import { Article } from 'src/app/models/articles.model';

@Injectable()
export class ArticleEffects {

  constructor(
    private actions$: Actions,
    private articlesService: ArticlesService) { }

  public getArticle$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ArticleActions.getArticleAction),
      map((action) => action.id),
      switchMap((id) => {
        return this.articlesService.getArticle(id).pipe(
          map((article: Article) => ArticleActions.getArticleSuccessAction({ payload: { article } })),
          catchError(() => of(ArticleActions.getArticleFailedAction()))
        );
      })
    )
  );
}
