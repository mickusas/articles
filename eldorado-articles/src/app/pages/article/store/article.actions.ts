import { createAction, props } from "@ngrx/store";
import { Article } from "src/app/models/articles.model";

export const GET_ARTICLE = "[Article] Get Article";
export const GET_ARTICLE_SUCCESS = "[Article] Get Article Success";
export const GET_ARTICLE_FAIL = "[Article] Get Article Fail";
export const RESET_ARTICLE = "[Article] Reset Article";

export const getArticleAction = createAction(
  GET_ARTICLE,
  props<{ id: number }>()
);
export const getArticleSuccessAction = createAction(
  GET_ARTICLE_SUCCESS,
  props<{
    payload: {
      article: Article;
    };
  }>()
);
export const getArticleFailedAction = createAction(GET_ARTICLE_FAIL);
export const resetArticleAction = createAction(RESET_ARTICLE);
