import { createReducer, on } from "@ngrx/store";
import { Article } from "src/app/models/articles.model";
import * as ArticleActions from "../store/article.actions";

export interface ArticleState {
  article: Article;
}

const initialState: ArticleState = {
  article: null
};

export const articleReducer = createReducer(
  initialState,
  on(ArticleActions.getArticleAction, (state, { id }) => ({
    ...state,
    id: id,
  })),
  on(ArticleActions.getArticleSuccessAction, (state, { payload }) => ({
    ...state,
    article: payload.article,
  })),
  on(ArticleActions.resetArticleAction, _state => (initialState))
);
