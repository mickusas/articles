import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MainArticleEditRoutingModule } from './main-article-edit-routing.module';
import { MainArticleEditComponent } from './main-article-edit.component';
import { ArticleViewModule } from 'src/app/shared/article-view/article-view/article-view.module';



@NgModule({
  declarations: [MainArticleEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MainArticleEditRoutingModule,
  ]
})
export class MainArticleEditModule { }
