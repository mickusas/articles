import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { environment } from "src/environments/environment";
import { Router } from "@angular/router";
import { Article } from "src/app/models/articles.model";
import { ARTICLE_LOCAL } from "src/app/shared/constants/article.const";

@Component({
  selector: "main-article-edit",
  templateUrl: "./main-article-edit.component.html",
  styleUrls: ["./main-article-edit.component.scss"]
})

export class MainArticleEditComponent implements OnInit {
  defaultImagePath: string = environment.imagePath;
  mainArticleEditFormGroup: FormGroup;

  constructor(
    private router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.initMainArticleEditFormGroup();
  }

  onSubmit() {
    const id: number = this.generateRandomID();
    const value: Article = this.mainArticleEditFormGroup.value;
    const articleLocal: Article = {
      id,
      title: value.title,
      imagePath: this.setArticleImage(value.imagePath),
      body: value.body
    };

    if (this.mainArticleEditFormGroup.valid) {
      localStorage.setItem(ARTICLE_LOCAL, JSON.stringify(articleLocal));
      this.router.navigate(['/articles/main-article']);
    }
  }

  initMainArticleEditFormGroup(): void {
    this.mainArticleEditFormGroup = this.fb.group({
      title: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(100)]],
      body: ['', [Validators.required, Validators.maxLength(300)]],
      imagePath: ['']
    });
  }

  generateRandomID(): number {
    return Math.random() * (9999999 - 11) + 11;
  }

  setArticleImage(imagePath: string): string {
    imagePath = imagePath.trim();

    if (
      imagePath === null ||
      imagePath === "" ||
      imagePath === undefined
    ) {
      return this.defaultImagePath;
    }

    return imagePath;
  }

}
