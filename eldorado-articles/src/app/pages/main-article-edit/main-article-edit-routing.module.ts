import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainArticleEditComponent } from './main-article-edit.component';

const routes: Routes = [
  {
    path: '',
    component: MainArticleEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainArticleEditRoutingModule { }
