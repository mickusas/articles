import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MainArticleComponent } from "./main-article.component";
import { MainArticleRoutingModule } from "./main-article-routing.module";
import { ArticleViewModule } from "src/app/shared/article-view/article-view/article-view.module";

@NgModule({
  declarations: [MainArticleComponent],
  imports: [
    CommonModule,
    MainArticleRoutingModule,
    ArticleViewModule
  ]
})
export class MainArticleModule { }
