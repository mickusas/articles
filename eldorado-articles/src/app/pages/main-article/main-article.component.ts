import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Article } from "src/app/models/articles.model";
import { ARTICLE_LOCAL } from "src/app/shared/constants/article.const";



@Component({
  selector: "app-main-article",
  templateUrl: "./main-article.component.html",
  styleUrls: ["./main-article.component.scss"]
})
export class MainArticleComponent implements OnInit {
  public article: Article;

  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
    this.article = JSON.parse(localStorage.getItem(ARTICLE_LOCAL));
    if (!this.article) {
      this.router.navigate(['/articles/submit']);
    }
  }

}
