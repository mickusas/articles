export interface Article {
  id: number;
  title: string;
  imagePath: string;
  body: string;
}

export interface ArticleView {
  title: string;
  imagePath: string;
  body: string;
}
