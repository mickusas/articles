import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleViewComponent } from './article-view.component';




@NgModule({
  declarations: [ArticleViewComponent],
  imports: [
    CommonModule
  ],
  exports: [ArticleViewComponent]
})
export class ArticleViewModule { }
