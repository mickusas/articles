import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input } from '@angular/core';
import { ArticleView } from 'src/app/models/articles.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-article-view',
  templateUrl: './article-view.component.html',
  styleUrls: ['./article-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ArticleViewComponent {
  @Input() article: ArticleView;

  defaultImagePath = environment.imagePath;

  constructor(
    private cRef: ChangeDetectorRef
  ) { }

  onClick(): void {
    this.cRef.detectChanges();
  }

}
