import { TruncatePipe } from './truncate.pipe';

fdescribe('TruncatePipe', () => {

  it(`Truncate less than value'`, () => {
    const pipe = new TruncatePipe();
    const value = '#'.repeat(9);
    const result = pipe.transform(value, [10]);
    expect(result).toBe(value);
  });

  it(`Truncate equal to value'`, () => {
    const pipe = new TruncatePipe();
    const value = '#'.repeat(10);
    const result = pipe.transform(value, [10]);
    expect(result).toBe(value);
  });

  it(`Truncate more than value'`, () => {
    const pipe = new TruncatePipe();
    const value = '#'.repeat(11);
    const result = pipe.transform(value, [10]);
    expect(result).toBe(value.substring(0, 10) + '...');
  });

});
