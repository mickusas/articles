import { ActionReducerMap } from "@ngrx/store";
import * as fromArticle from "../pages/article/store/article.reducer";
import * as fromArticles from "../pages/articles/store/articles.reducer";


export interface AppState {
  article: fromArticle.ArticleState;
  articles: fromArticles.ArticlesState;
}

export const appReducer: ActionReducerMap<AppState> = {
  article: fromArticle.articleReducer,
  articles: fromArticles.articlesReducer
};
