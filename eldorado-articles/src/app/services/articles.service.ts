import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { Article } from "../models/articles.model";
import { environment } from "src/environments/environment";

@Injectable()
export class ArticlesService {
  postsUrl = `${environment.postsUrl}`;

  constructor(private http: HttpClient) { }

  getArticle(id: number): Observable<Article> {
    return this.http.get<Article>(`${this.postsUrl}/${id}`)
  }

  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(`${this.postsUrl}?_sort=id&_order=desc&_limit=10`)
  }

}
